﻿namespace PatchManager
{
    partial class FormCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCreator));
            this.listPatchu = new System.Windows.Forms.ListBox();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPatchDateYear = new System.Windows.Forms.TextBox();
            this.tbPatchDateMonth = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnPatchTypeHelp = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.chbHidePatch = new System.Windows.Forms.CheckBox();
            this.tbPatchCaption = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboPatchType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbPatchText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPatchAuthor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPatchPatchFile = new System.Windows.Forms.TextBox();
            this.tbPatchDestFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPatchDateDay = new System.Windows.Forms.TextBox();
            this.tbPatchNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSavePatch = new System.Windows.Forms.Button();
            this.btnNewPatch = new System.Windows.Forms.Button();
            this.btnDelPatch = new System.Windows.Forms.Button();
            this.lblPatcherVer = new System.Windows.Forms.Label();
            this.tbVersion = new System.Windows.Forms.TextBox();
            this.chbDisablePatch = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listPatchu
            // 
            this.listPatchu.FormattingEnabled = true;
            this.listPatchu.Location = new System.Drawing.Point(12, 12);
            this.listPatchu.Name = "listPatchu";
            this.listPatchu.Size = new System.Drawing.Size(234, 394);
            this.listPatchu.TabIndex = 0;
            this.listPatchu.SelectedIndexChanged += new System.EventHandler(this.listPatchu_SelectedIndexChanged);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.Location = new System.Drawing.Point(252, 384);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(89, 23);
            this.btnLoadFile.TabIndex = 1;
            this.btnLoadFile.Text = "Načíst soubor";
            this.btnLoadFile.UseVisualStyleBackColor = true;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chbDisablePatch);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tbPatchDateYear);
            this.panel1.Controls.Add(this.tbPatchDateMonth);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btnPatchTypeHelp);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Controls.Add(this.chbHidePatch);
            this.panel1.Controls.Add(this.tbPatchCaption);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.comboPatchType);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tbPatchText);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tbPatchAuthor);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tbPatchPatchFile);
            this.panel1.Controls.Add(this.tbPatchDestFile);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbPatchDateDay);
            this.panel1.Controls.Add(this.tbPatchNum);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(252, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(347, 340);
            this.panel1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(208, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = ".";
            // 
            // tbPatchDateYear
            // 
            this.tbPatchDateYear.Location = new System.Drawing.Point(224, 56);
            this.tbPatchDateYear.Name = "tbPatchDateYear";
            this.tbPatchDateYear.Size = new System.Drawing.Size(118, 20);
            this.tbPatchDateYear.TabIndex = 7;
            // 
            // tbPatchDateMonth
            // 
            this.tbPatchDateMonth.Location = new System.Drawing.Point(155, 56);
            this.tbPatchDateMonth.Name = "tbPatchDateMonth";
            this.tbPatchDateMonth.Size = new System.Drawing.Size(47, 20);
            this.tbPatchDateMonth.TabIndex = 6;
            this.tbPatchDateMonth.TextChanged += new System.EventHandler(this.tbPatchDateMonth_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(139, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = ".";
            // 
            // btnPatchTypeHelp
            // 
            this.btnPatchTypeHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPatchTypeHelp.Location = new System.Drawing.Point(324, 29);
            this.btnPatchTypeHelp.Name = "btnPatchTypeHelp";
            this.btnPatchTypeHelp.Size = new System.Drawing.Size(18, 21);
            this.btnPatchTypeHelp.TabIndex = 21;
            this.btnPatchTypeHelp.Text = "?";
            this.btnPatchTypeHelp.UseVisualStyleBackColor = true;
            this.btnPatchTypeHelp.Click += new System.EventHandler(this.btnPatchTypeHelp_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(142, 308);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 14;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chbHidePatch
            // 
            this.chbHidePatch.AutoSize = true;
            this.chbHidePatch.Location = new System.Drawing.Point(88, 285);
            this.chbHidePatch.Name = "chbHidePatch";
            this.chbHidePatch.Size = new System.Drawing.Size(80, 17);
            this.chbHidePatch.TabIndex = 13;
            this.chbHidePatch.Text = "Skrýt patch";
            this.chbHidePatch.UseVisualStyleBackColor = true;
            // 
            // tbPatchCaption
            // 
            this.tbPatchCaption.Location = new System.Drawing.Point(88, 160);
            this.tbPatchCaption.Name = "tbPatchCaption";
            this.tbPatchCaption.Size = new System.Drawing.Size(254, 20);
            this.tbPatchCaption.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 163);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Nadpis";
            // 
            // comboPatchType
            // 
            this.comboPatchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPatchType.FormattingEnabled = true;
            this.comboPatchType.Items.AddRange(new object[] {
            "invalid",
            "rewrite",
            "delete",
            "patch",
            "text"});
            this.comboPatchType.Location = new System.Drawing.Point(88, 29);
            this.comboPatchType.Name = "comboPatchType";
            this.comboPatchType.Size = new System.Drawing.Size(230, 21);
            this.comboPatchType.TabIndex = 4;
            this.comboPatchType.SelectedIndexChanged += new System.EventHandler(this.comboPatchType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Text";
            // 
            // tbPatchText
            // 
            this.tbPatchText.Location = new System.Drawing.Point(88, 186);
            this.tbPatchText.Multiline = true;
            this.tbPatchText.Name = "tbPatchText";
            this.tbPatchText.Size = new System.Drawing.Size(254, 93);
            this.tbPatchText.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Autor";
            // 
            // tbPatchAuthor
            // 
            this.tbPatchAuthor.Location = new System.Drawing.Point(88, 134);
            this.tbPatchAuthor.Name = "tbPatchAuthor";
            this.tbPatchAuthor.Size = new System.Drawing.Size(254, 20);
            this.tbPatchAuthor.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Patch soubor";
            // 
            // tbPatchPatchFile
            // 
            this.tbPatchPatchFile.Location = new System.Drawing.Point(88, 108);
            this.tbPatchPatchFile.Name = "tbPatchPatchFile";
            this.tbPatchPatchFile.Size = new System.Drawing.Size(254, 20);
            this.tbPatchPatchFile.TabIndex = 9;
            // 
            // tbPatchDestFile
            // 
            this.tbPatchDestFile.Location = new System.Drawing.Point(88, 82);
            this.tbPatchDestFile.Name = "tbPatchDestFile";
            this.tbPatchDestFile.Size = new System.Drawing.Size(254, 20);
            this.tbPatchDestFile.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cílový soubor";
            // 
            // tbPatchDateDay
            // 
            this.tbPatchDateDay.Location = new System.Drawing.Point(88, 56);
            this.tbPatchDateDay.Name = "tbPatchDateDay";
            this.tbPatchDateDay.Size = new System.Drawing.Size(45, 20);
            this.tbPatchDateDay.TabIndex = 5;
            this.tbPatchDateDay.TextChanged += new System.EventHandler(this.tbPatchDateDay_TextChanged);
            // 
            // tbPatchNum
            // 
            this.tbPatchNum.Location = new System.Drawing.Point(88, 3);
            this.tbPatchNum.Name = "tbPatchNum";
            this.tbPatchNum.Size = new System.Drawing.Size(254, 20);
            this.tbPatchNum.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Datum patche";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Typ patche";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Číslo patche";
            // 
            // btnSavePatch
            // 
            this.btnSavePatch.Location = new System.Drawing.Point(347, 384);
            this.btnSavePatch.Name = "btnSavePatch";
            this.btnSavePatch.Size = new System.Drawing.Size(64, 23);
            this.btnSavePatch.TabIndex = 4;
            this.btnSavePatch.Text = "Uložit";
            this.btnSavePatch.UseVisualStyleBackColor = true;
            this.btnSavePatch.Click += new System.EventHandler(this.btnSavePatch_Click);
            // 
            // btnNewPatch
            // 
            this.btnNewPatch.Location = new System.Drawing.Point(417, 384);
            this.btnNewPatch.Name = "btnNewPatch";
            this.btnNewPatch.Size = new System.Drawing.Size(88, 23);
            this.btnNewPatch.TabIndex = 3;
            this.btnNewPatch.Text = "Nový patch";
            this.btnNewPatch.UseVisualStyleBackColor = true;
            this.btnNewPatch.Click += new System.EventHandler(this.btnNewPatch_Click);
            // 
            // btnDelPatch
            // 
            this.btnDelPatch.Location = new System.Drawing.Point(511, 384);
            this.btnDelPatch.Name = "btnDelPatch";
            this.btnDelPatch.Size = new System.Drawing.Size(88, 23);
            this.btnDelPatch.TabIndex = 4;
            this.btnDelPatch.Text = "Smazat patch";
            this.btnDelPatch.UseVisualStyleBackColor = true;
            this.btnDelPatch.Click += new System.EventHandler(this.btnDelPatch_Click);
            // 
            // lblPatcherVer
            // 
            this.lblPatcherVer.AutoSize = true;
            this.lblPatcherVer.Location = new System.Drawing.Point(252, 361);
            this.lblPatcherVer.Name = "lblPatcherVer";
            this.lblPatcherVer.Size = new System.Drawing.Size(82, 13);
            this.lblPatcherVer.TabIndex = 5;
            this.lblPatcherVer.Text = "Verze patcheru:";
            // 
            // tbVersion
            // 
            this.tbVersion.Location = new System.Drawing.Point(340, 358);
            this.tbVersion.Name = "tbVersion";
            this.tbVersion.Size = new System.Drawing.Size(100, 20);
            this.tbVersion.TabIndex = 6;
            // 
            // chbDisablePatch
            // 
            this.chbDisablePatch.AutoSize = true;
            this.chbDisablePatch.Location = new System.Drawing.Point(174, 285);
            this.chbDisablePatch.Name = "chbDisablePatch";
            this.chbDisablePatch.Size = new System.Drawing.Size(83, 17);
            this.chbDisablePatch.TabIndex = 26;
            this.chbDisablePatch.Text = "Zrušit Patch";
            this.chbDisablePatch.UseVisualStyleBackColor = true;
            // 
            // FormCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 417);
            this.Controls.Add(this.tbVersion);
            this.Controls.Add(this.lblPatcherVer);
            this.Controls.Add(this.btnDelPatch);
            this.Controls.Add(this.btnNewPatch);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSavePatch);
            this.Controls.Add(this.btnLoadFile);
            this.Controls.Add(this.listPatchu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormCreator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reborn patch editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCreator_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listPatchu;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPatchDestFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPatchDateDay;
        private System.Windows.Forms.TextBox tbPatchNum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbPatchText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPatchAuthor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPatchPatchFile;
        private System.Windows.Forms.Button btnSavePatch;
        private System.Windows.Forms.Button btnNewPatch;
        private System.Windows.Forms.ComboBox comboPatchType;
        private System.Windows.Forms.TextBox tbPatchCaption;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnDelPatch;
        private System.Windows.Forms.CheckBox chbHidePatch;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnPatchTypeHelp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPatchDateYear;
        private System.Windows.Forms.TextBox tbPatchDateMonth;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPatcherVer;
        private System.Windows.Forms.TextBox tbVersion;
        private System.Windows.Forms.CheckBox chbDisablePatch;
    }
}

