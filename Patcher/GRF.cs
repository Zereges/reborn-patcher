﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace Patcher
{
    /// <summary>
    /// Struktura Hlavicky GRF Souboru</summary>
    /// <remarks>
    /// Size: 46 bytes
    /// struct GRF_Header {                    
    ///     unsigned char signature[16];       0
    ///     unsigned char allowEncryption[14]; 16
    ///     uint32 fileTableOffset;            30
    ///     uint32 number1;                    34
    ///     uint32 number2;                    38
    ///     uint32 version;                    42
    /// };
    /// </remarks>
    public struct GRFHeader
    {
        public char[] signature, encrypt;
        public UInt32 filetableoffset, num1, num2, version;

        public GRFHeader(char[] signature, char[] encrypt, UInt32 FToffset, UInt32 num1, UInt32 num2, UInt32 ver)
        {
            this.signature = signature;
            this.encrypt = encrypt;
            this.filetableoffset = FToffset;
            this.num1 = num1;
            this.num2 = num2;
            this.version = ver;
        }

    }

    /// <summary>
    /// Struktura informace o souboru. Jmeno, jak je velky, kde je umisten,..
    /// </summary>
    public struct GRFFileInfo
    {
        public byte[] filename;
        public UInt32 complen;
        public UInt32 complen_align;
        public UInt32 uncomp_len;
        public byte flags;
        public UInt32 offset;

        public GRFFileInfo(byte[] filename, UInt32 cl, UInt32 cl_a, UInt32 uncl, byte flags, UInt32 offset)
        {
            this.filename = filename;
            this.complen = cl;
            this.complen_align = cl_a;
            this.uncomp_len = uncl;
            this.flags = flags;
            this.offset = offset;
        }
    }

    /// <summary>
    /// Struktura GRF hlavicky - offset, velikost a pole informaci o souboru(GRFFileInfo)
    /// </summary>
    public struct GRFFileTable
    {
        public UInt32 comp_len, uncomp_len, offset;
        public GRFFileInfo[] fileinfo;
        public int errcode;
        public String errtext;

    }

    /// <summary>
    /// Trida funkci pracujicich s GRF souborem
    /// </summary>
    public class GRF
    {
        /// <summary>
        /// Funkce vracejici GRF Header.
        /// </summary>
        /// <param name="filename">jmeno souboru (absolutni nebo relativni)</param>
        /// <returns>Hlavicka GRF souboru</returns>
        public static GRFHeader GetFileHeader(String filename)
        {
            FileStream fs = null;
            BinaryReader br = null;
            GRFHeader head = new GRFHeader();
            try
            {

                if (File.Exists(filename))
                {
                    fs = File.OpenRead(filename);
                    br = new BinaryReader(fs);
                    head = new GRFHeader(br.ReadChars(16), br.ReadChars(14), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32());
                    //if (head.version < 0x200) head = new GRFHeader();
                }   
            }
            finally
            {
                if(fs != null) br.Close();
                if(br != null) fs.Close();
            }
            return head;

        }

        /// <summary>
        /// Funkce vracejici tabulku souboru. Nacte ji z GRF souboru, dekomprimuje a naplni s nimi strukturu.
        /// </summary>
        /// <param name="filename">jmeno souboru</param>
        /// <returns>Tabulku souboru v GRF archivu</returns>
        public static GRFFileTable GetFileTable(String filename)
        {
            GRFFileTable ft = new GRFFileTable();
            FileStream fss = null;
            BinaryReader br = null;
            MemoryStream msin = null;
            MemoryStream msout = null;
            InflaterInputStream decomp = null;
            try
            {
                if (!File.Exists(filename)) 
                    throw new Exception("Soubor '" + filename + "' nebyl nalezen.");

                GRFHeader head = GetFileHeader(filename);

                if (head.version < 0x200) 
                    throw new Exception("Špatná verze GRF. GRF Rebornu musí být verze 2.");

                fss = File.OpenRead(filename);
                br = new BinaryReader(fss);

                ft.fileinfo = new GRFFileInfo[head.num2 - head.num1 - 7];
                ft.offset = head.filetableoffset;
                br.BaseStream.Seek(ft.offset + 46, SeekOrigin.Begin);
                ft.comp_len = br.ReadUInt32();
                ft.uncomp_len = br.ReadUInt32();

                if ((int)ft.comp_len <= 0) 
                    throw new Exception("Máš poškozený soubor '" + filename + "'. V možnostech nastavení klikni na možnost 'Reset Patchů'.");

                byte[] table_body = br.ReadBytes((int)ft.comp_len);
                byte[] table_uncomp = new byte[ft.uncomp_len];
                fss.Close();
                br.Close();
                msin = new MemoryStream(table_body);
                msout = new MemoryStream(table_uncomp);
                decomp = new InflaterInputStream(msin);

                byte[] buffer = new byte[8000];
                int len;
                while ((len = decomp.Read(buffer, 0, buffer.Length)) > 0)
                {
                    msout.Write(buffer, 0, len);
                }
                decomp.Close();
                msin.Close();
                msout.Close();

                msin = new MemoryStream(table_uncomp);
                br = new BinaryReader(msin);
                for (int i = 0; i < ft.fileinfo.Length; i++)
                {
                    int name_length = 0;
                    for (int p = (int)br.BaseStream.Position; table_uncomp[p] != 0; p++)
                        name_length++;
                    ft.fileinfo[i] = new GRFFileInfo();
                    ft.fileinfo[i].filename = br.ReadBytes(name_length);
                    br.ReadByte();
                    ft.fileinfo[i].complen = br.ReadUInt32();
                    ft.fileinfo[i].complen_align = br.ReadUInt32();
                    ft.fileinfo[i].uncomp_len = br.ReadUInt32();
                    ft.fileinfo[i].flags = br.ReadByte();
                    ft.fileinfo[i].offset = br.ReadUInt32();
                }
                br.Close();
                msin.Close();
                
            }
            catch (Exception ex)
            {
                ft.errcode = 1;
                ft.errtext = ex.Message + " " + ex.StackTrace;
            }
            finally
            {
                if(msin != null) msin.Close();
                if(msout != null) msout.Close();
                if(fss != null) fss.Close();
                if(br != null) br.Close();
                if(decomp != null) decomp.Close();
            }
            return ft;

        }

        /// <summary>
        /// Funkce extrahujici soubor z GRF archivu
        /// </summary>
        /// <param name="filename">jmeno GRF archivu</param>
        /// <param name="offset">offset v GRF archivu od zacatku souboru</param>
        /// <param name="comp_len">komprimovana velikost</param>
        /// <param name="uncomp_len">dekomprimovana velikost</param>
        /// <param name="dest_path">jmeno ciloveho extrahovaneho souboru</param>
        /// <returns>0 = uspesne extrahovano, 1 = chyba</returns>
        public static int ExtractFile(String filename, UInt32 offset, UInt32 comp_len, UInt32 uncomp_len, String dest_path)
        {
            FileStream fs = null;
            BinaryReader br = null;
            MemoryStream msin = null;
            InflaterInputStream decomp = null;
            BinaryWriter wr = null;
            int vystup = 0;
            try
            {
                if (!File.Exists(filename)) throw new Exception("Soubor Neexistuje");
                if (offset == 1326 && uncomp_len == 1372) throw new Exception("Je to adresar");
                fs = File.OpenRead(filename);
                br = new BinaryReader(fs);
                String dir = Path.GetDirectoryName(dest_path);
                if (dir.Length > 0 && !Directory.Exists(dir)) Directory.CreateDirectory(dir);

                br.BaseStream.Seek(46 + offset, SeekOrigin.Begin);
                byte[] data = br.ReadBytes((int)comp_len);
                br.Close();
                msin = new MemoryStream(data);
                decomp = new InflaterInputStream(msin);
                wr = new BinaryWriter(new FileStream(dest_path, FileMode.CreateNew));

                byte[] buffer = new byte[8000];
                int len;
                while ((len = msin.Read(buffer, 0, buffer.Length)) > 0)
                {
                    wr.Write(buffer, 0, len);
                }
                wr.Close();
                decomp.Close();
                msin.Close();
                vystup = 0;
            }
            catch (Exception)
            {
                vystup = 1;
            }
            finally
            {
                if(fs != null) fs.Close();
                if(br != null) br.Close();
                if(msin != null) msin.Close();
                if(decomp != null) decomp.Close();
                if(wr != null) wr.Close();

            }
            return vystup;

        }

        /// <summary>
        /// Vytvori a zkomprimuje hlavicku souboru a vrati bitovou reprezentaci vhodnou pro zapis do souboru.
        /// </summary>
        /// <param name="filetable">tabulka souboru</param>
        /// <returns>bitova reprezentace tabulky souboru</returns>
        public static byte[] MakeFileTable(GRFFileTable filetable)
        {
            byte[] output_data = null;
            MemoryStream ms = null;
            DeflaterOutputStream compress = null;

            try
            {
                int uncomp_len = 0;
                foreach (GRFFileInfo fi in filetable.fileinfo)
                {
                    uncomp_len += fi.filename.Length + 1;
                }
                uncomp_len += filetable.fileinfo.Length * 17;

                byte[] uncomp_data = new byte[uncomp_len];
                ms = new MemoryStream(uncomp_data);
                foreach (GRFFileInfo fi in filetable.fileinfo)
                {
                    ms.Write(fi.filename, 0, fi.filename.Length);
                    ms.WriteByte(0); // terminating \0 (null)
                    ms.Write(BitConverter.GetBytes(fi.complen), 0, 4); // UInt32
                    ms.Write(BitConverter.GetBytes(fi.complen_align), 0, 4); // UInt32
                    ms.Write(BitConverter.GetBytes(fi.uncomp_len), 0, 4); // UInt32
                    ms.Write(BitConverter.GetBytes(fi.flags), 0, 1); // Byte
                    ms.Write(BitConverter.GetBytes(fi.offset), 0, 4); // UInt32
                }
                ms.Close();
                ms = new MemoryStream();
                compress = new DeflaterOutputStream(ms);
                compress.Write(uncomp_data, 0, uncomp_data.Length);
                compress.Close();
                byte[] compressed = ms.ToArray();
                ms.Close();
                output_data = new byte[compressed.Length + 8];
                ms = new MemoryStream(output_data);
                ms.Write(BitConverter.GetBytes(compressed.Length), 0, 4);
                ms.Write(BitConverter.GetBytes(uncomp_len), 0, 4);
                ms.Close();
                Buffer.BlockCopy(compressed, 0, output_data, 8, compressed.Length);
            }
            catch (Exception)
            {
                output_data = null;
            }
            finally
            {
                if(ms != null) ms.Close();
                if(compress != null) compress.Close();
            }
            return output_data;
        }

        /// <summary>
        /// Funkce vytvarejici tabulku souboru
        /// </summary>
        /// <param name="filetableoffset">offset tabulky souboru (na konci souboru)</param>
        /// <param name="filecount">pocet souboru v GRF archivu</param>
        /// <returns>bitova reprezentace hlavicky GRF archivu<56/returns>
        public static byte[] MakeFileHeader(int filetableoffset, int filecount)
        {
            byte[] data = null;
            MemoryStream ms = null;
            try
            {
                data = new byte[46];
                ms = new MemoryStream(data);
                ms.Write(Encoding.Default.GetBytes("Master of Magic\0"), 0, 16);
                for (byte i = 1; i <= 14; i++)
                {
                    ms.Write(BitConverter.GetBytes(i), 0, 1);
                }
                //ms.Seek(14, SeekOrigin.Current);
                ms.Write(BitConverter.GetBytes(filetableoffset), 0, 4);
                ms.Write(BitConverter.GetBytes(10), 0, 4);
                ms.Write(BitConverter.GetBytes(17 + filecount), 0, 4);
                ms.Write(BitConverter.GetBytes(0x200), 0, 4);
            }
            catch (Exception)
            {
                data = null;
            }
            finally
            {
               if(ms != null) ms.Close();
            }
            return data;
        }
    }
}
