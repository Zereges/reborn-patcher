﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Patcher
{
    public partial class LoginDialog : Form
    {
        public string login = "", password = "";


        public LoginDialog()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            OK();
        }

        private void OK()
        {
            this.DialogResult = DialogResult.OK;
            this.login = tbLogin.Text;
            this.password = tbPass.Text;
            this.Close();
        }

        private void tbLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13) OK();
        }
    }
}
