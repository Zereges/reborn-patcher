﻿namespace Patcher
{
    partial class formSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formSettings));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSetupExe = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSdataReset = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblWeb = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblMap = new System.Windows.Forms.Label();
            this.lblChar = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPortTest = new System.Windows.Forms.Button();
            this.tbInfo2 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRegReset = new System.Windows.Forms.Button();
            this.gbTrouble = new System.Windows.Forms.GroupBox();
            this.btnPatchReset = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.antishake = new System.Windows.Forms.CheckBox();
            this.gbPets = new System.Windows.Forms.GroupBox();
            this.rbPetsCzech = new System.Windows.Forms.RadioButton();
            this.rbPetsOff = new System.Windows.Forms.RadioButton();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.gbScreen = new System.Windows.Forms.GroupBox();
            this.rbScreenFullScreen = new System.Windows.Forms.RadioButton();
            this.rbScreenWindow = new System.Windows.Forms.RadioButton();
            this.gbGrEf = new System.Windows.Forms.GroupBox();
            this.rbGrEfGW = new System.Windows.Forms.RadioButton();
            this.rbGrEfOsek = new System.Windows.Forms.RadioButton();
            this.rbGrEfStandard = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbTrouble.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbPets.SuspendLayout();
            this.gbScreen.SuspendLayout();
            this.gbGrEf.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(384, 449);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Nastavení hry";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSetupExe);
            this.groupBox5.Location = new System.Drawing.Point(24, 16);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(336, 62);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Chceš změnit nastavení hry?";
            // 
            // btnSetupExe
            // 
            this.btnSetupExe.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSetupExe.Location = new System.Drawing.Point(167, 20);
            this.btnSetupExe.Margin = new System.Windows.Forms.Padding(4);
            this.btnSetupExe.Name = "btnSetupExe";
            this.btnSetupExe.Size = new System.Drawing.Size(160, 28);
            this.btnSetupExe.TabIndex = 22;
            this.btnSetupExe.Text = "&Spustit Setup";
            this.btnSetupExe.UseVisualStyleBackColor = true;
            this.btnSetupExe.Click += new System.EventHandler(this.btnSetupExe_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.tbInfo2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.gbTrouble);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(384, 449);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Řešení problémů";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSdataReset);
            this.groupBox3.Location = new System.Drawing.Point(24, 155);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(336, 62);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hra náhodně padá...";
            // 
            // btnSdataReset
            // 
            this.btnSdataReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSdataReset.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSdataReset.Location = new System.Drawing.Point(167, 23);
            this.btnSdataReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnSdataReset.Name = "btnSdataReset";
            this.btnSdataReset.Size = new System.Drawing.Size(160, 28);
            this.btnSdataReset.TabIndex = 35;
            this.btnSdataReset.Text = "Reset &data.grf";
            this.btnSdataReset.UseVisualStyleBackColor = false;
            this.btnSdataReset.Click += new System.EventHandler(this.btnSdataReset_Click);
            this.btnSdataReset.MouseEnter += new System.EventHandler(this.btnSdataReset_MouseEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblWeb);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.lblMap);
            this.groupBox2.Controls.Add(this.lblChar);
            this.groupBox2.Controls.Add(this.lblLogin);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnPortTest);
            this.groupBox2.Location = new System.Drawing.Point(24, 225);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(336, 119);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nelze se připojit k serveru?";
            // 
            // lblWeb
            // 
            this.lblWeb.AutoSize = true;
            this.lblWeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblWeb.Location = new System.Drawing.Point(64, 94);
            this.lblWeb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWeb.Name = "lblWeb";
            this.lblWeb.Size = new System.Drawing.Size(31, 17);
            this.lblWeb.TabIndex = 34;
            this.lblWeb.Text = "n/a";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 33;
            this.label4.Text = "Web:";
            // 
            // lblMap
            // 
            this.lblMap.AutoSize = true;
            this.lblMap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMap.Location = new System.Drawing.Point(64, 78);
            this.lblMap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMap.Name = "lblMap";
            this.lblMap.Size = new System.Drawing.Size(31, 17);
            this.lblMap.TabIndex = 32;
            this.lblMap.Text = "n/a";
            // 
            // lblChar
            // 
            this.lblChar.AutoSize = true;
            this.lblChar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblChar.Location = new System.Drawing.Point(64, 62);
            this.lblChar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChar.Name = "lblChar";
            this.lblChar.Size = new System.Drawing.Size(31, 17);
            this.lblChar.TabIndex = 31;
            this.lblChar.Text = "n/a";
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLogin.Location = new System.Drawing.Point(64, 46);
            this.lblLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(31, 17);
            this.lblLogin.TabIndex = 30;
            this.lblLogin.Text = "n/a";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "Map:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 17);
            this.label2.TabIndex = 28;
            this.label2.Text = "Char:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "Login:";
            // 
            // btnPortTest
            // 
            this.btnPortTest.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnPortTest.Location = new System.Drawing.Point(167, 81);
            this.btnPortTest.Margin = new System.Windows.Forms.Padding(4);
            this.btnPortTest.Name = "btnPortTest";
            this.btnPortTest.Size = new System.Drawing.Size(160, 28);
            this.btnPortTest.TabIndex = 26;
            this.btnPortTest.Text = "&Test Portů";
            this.btnPortTest.UseVisualStyleBackColor = true;
            this.btnPortTest.Click += new System.EventHandler(this.btnPortTest_Click);
            this.btnPortTest.MouseEnter += new System.EventHandler(this.btnPortTest_MouseEnter);
            // 
            // tbInfo2
            // 
            this.tbInfo2.Location = new System.Drawing.Point(24, 354);
            this.tbInfo2.Margin = new System.Windows.Forms.Padding(4);
            this.tbInfo2.Multiline = true;
            this.tbInfo2.Name = "tbInfo2";
            this.tbInfo2.ReadOnly = true;
            this.tbInfo2.Size = new System.Drawing.Size(335, 75);
            this.tbInfo2.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRegReset);
            this.groupBox1.Location = new System.Drawing.Point(24, 85);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(336, 62);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hra nejde vůbec spustit?";
            // 
            // btnRegReset
            // 
            this.btnRegReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnRegReset.Location = new System.Drawing.Point(167, 23);
            this.btnRegReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegReset.Name = "btnRegReset";
            this.btnRegReset.Size = new System.Drawing.Size(160, 28);
            this.btnRegReset.TabIndex = 17;
            this.btnRegReset.Text = "&Smazat Registry";
            this.btnRegReset.UseVisualStyleBackColor = true;
            this.btnRegReset.Click += new System.EventHandler(this.btnRegReset_Click);
            this.btnRegReset.MouseEnter += new System.EventHandler(this.btnRegReset_MouseEnter);
            // 
            // gbTrouble
            // 
            this.gbTrouble.Controls.Add(this.btnPatchReset);
            this.gbTrouble.Location = new System.Drawing.Point(24, 16);
            this.gbTrouble.Margin = new System.Windows.Forms.Padding(4);
            this.gbTrouble.Name = "gbTrouble";
            this.gbTrouble.Padding = new System.Windows.Forms.Padding(4);
            this.gbTrouble.Size = new System.Drawing.Size(336, 62);
            this.gbTrouble.TabIndex = 16;
            this.gbTrouble.TabStop = false;
            this.gbTrouble.Text = "Hra píše server odpojen?";
            // 
            // btnPatchReset
            // 
            this.btnPatchReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnPatchReset.Location = new System.Drawing.Point(167, 20);
            this.btnPatchReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnPatchReset.Name = "btnPatchReset";
            this.btnPatchReset.Size = new System.Drawing.Size(160, 28);
            this.btnPatchReset.TabIndex = 1;
            this.btnPatchReset.Text = "&Resetovat Patche";
            this.btnPatchReset.UseVisualStyleBackColor = true;
            this.btnPatchReset.Click += new System.EventHandler(this.btnPatchReset_Click);
            this.btnPatchReset.MouseEnter += new System.EventHandler(this.btnPatchReset_MouseEnter);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.gbPets);
            this.tabPage1.Controls.Add(this.tbInfo);
            this.tabPage1.Controls.Add(this.gbScreen);
            this.tabPage1.Controls.Add(this.gbGrEf);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(384, 449);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nastavení Rebornu";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.antishake);
            this.groupBox4.Location = new System.Drawing.Point(192, 142);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(168, 118);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ostatní";
            // 
            // antishake
            // 
            this.antishake.AutoSize = true;
            this.antishake.Location = new System.Drawing.Point(13, 23);
            this.antishake.Margin = new System.Windows.Forms.Padding(4);
            this.antishake.Name = "antishake";
            this.antishake.Size = new System.Drawing.Size(97, 21);
            this.antishake.TabIndex = 15;
            this.antishake.Text = "Anti-shake";
            this.antishake.UseVisualStyleBackColor = true;
            this.antishake.CheckedChanged += new System.EventHandler(this.antishake_CheckedChanged);
            this.antishake.MouseEnter += new System.EventHandler(this.antishake_MouseEnter);
            // 
            // gbPets
            // 
            this.gbPets.Controls.Add(this.rbPetsCzech);
            this.gbPets.Controls.Add(this.rbPetsOff);
            this.gbPets.Location = new System.Drawing.Point(24, 142);
            this.gbPets.Margin = new System.Windows.Forms.Padding(4);
            this.gbPets.Name = "gbPets";
            this.gbPets.Padding = new System.Windows.Forms.Padding(4);
            this.gbPets.Size = new System.Drawing.Size(160, 118);
            this.gbPets.TabIndex = 9;
            this.gbPets.TabStop = false;
            this.gbPets.Text = "Hlášky petů";
            // 
            // rbPetsCzech
            // 
            this.rbPetsCzech.AutoSize = true;
            this.rbPetsCzech.Checked = true;
            this.rbPetsCzech.Location = new System.Drawing.Point(7, 20);
            this.rbPetsCzech.Margin = new System.Windows.Forms.Padding(4);
            this.rbPetsCzech.Name = "rbPetsCzech";
            this.rbPetsCzech.Size = new System.Drawing.Size(137, 21);
            this.rbPetsCzech.TabIndex = 1;
            this.rbPetsCzech.TabStop = true;
            this.rbPetsCzech.Text = "Přeložené hlášky";
            this.rbPetsCzech.UseVisualStyleBackColor = true;
            this.rbPetsCzech.MouseEnter += new System.EventHandler(this.rbPetsCzech_MouseEnter);
            // 
            // rbPetsOff
            // 
            this.rbPetsOff.AutoSize = true;
            this.rbPetsOff.Location = new System.Drawing.Point(7, 48);
            this.rbPetsOff.Margin = new System.Windows.Forms.Padding(4);
            this.rbPetsOff.Name = "rbPetsOff";
            this.rbPetsOff.Size = new System.Drawing.Size(81, 21);
            this.rbPetsOff.TabIndex = 0;
            this.rbPetsOff.Text = "Vypnuto";
            this.rbPetsOff.UseVisualStyleBackColor = true;
            this.rbPetsOff.MouseEnter += new System.EventHandler(this.rbPetsOff_MouseEnter);
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(24, 354);
            this.tbInfo.Margin = new System.Windows.Forms.Padding(4);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.Size = new System.Drawing.Size(335, 75);
            this.tbInfo.TabIndex = 14;
            // 
            // gbScreen
            // 
            this.gbScreen.Controls.Add(this.rbScreenFullScreen);
            this.gbScreen.Controls.Add(this.rbScreenWindow);
            this.gbScreen.Location = new System.Drawing.Point(192, 16);
            this.gbScreen.Margin = new System.Windows.Forms.Padding(4);
            this.gbScreen.Name = "gbScreen";
            this.gbScreen.Padding = new System.Windows.Forms.Padding(4);
            this.gbScreen.Size = new System.Drawing.Size(168, 118);
            this.gbScreen.TabIndex = 11;
            this.gbScreen.TabStop = false;
            this.gbScreen.Text = "Obrazovka";
            this.gbScreen.Visible = false;
            // 
            // rbScreenFullScreen
            // 
            this.rbScreenFullScreen.AutoSize = true;
            this.rbScreenFullScreen.Location = new System.Drawing.Point(7, 48);
            this.rbScreenFullScreen.Margin = new System.Windows.Forms.Padding(4);
            this.rbScreenFullScreen.Name = "rbScreenFullScreen";
            this.rbScreenFullScreen.Size = new System.Drawing.Size(100, 21);
            this.rbScreenFullScreen.TabIndex = 2;
            this.rbScreenFullScreen.Text = "Full Screen";
            this.rbScreenFullScreen.UseVisualStyleBackColor = true;
            this.rbScreenFullScreen.CheckedChanged += new System.EventHandler(this.rbScreenFullScreen_CheckedChanged);
            this.rbScreenFullScreen.MouseEnter += new System.EventHandler(this.rbScreenFullScreen_MouseEnter);
            // 
            // rbScreenWindow
            // 
            this.rbScreenWindow.AutoSize = true;
            this.rbScreenWindow.Checked = true;
            this.rbScreenWindow.Location = new System.Drawing.Point(7, 20);
            this.rbScreenWindow.Margin = new System.Windows.Forms.Padding(4);
            this.rbScreenWindow.Name = "rbScreenWindow";
            this.rbScreenWindow.Size = new System.Drawing.Size(63, 21);
            this.rbScreenWindow.TabIndex = 0;
            this.rbScreenWindow.TabStop = true;
            this.rbScreenWindow.Text = "Okno";
            this.rbScreenWindow.UseVisualStyleBackColor = true;
            this.rbScreenWindow.CheckedChanged += new System.EventHandler(this.rbScreenWindow_CheckedChanged);
            this.rbScreenWindow.MouseEnter += new System.EventHandler(this.rbScreenWindow_MouseEnter);
            // 
            // gbGrEf
            // 
            this.gbGrEf.Controls.Add(this.rbGrEfGW);
            this.gbGrEf.Controls.Add(this.rbGrEfOsek);
            this.gbGrEf.Controls.Add(this.rbGrEfStandard);
            this.gbGrEf.Location = new System.Drawing.Point(24, 16);
            this.gbGrEf.Margin = new System.Windows.Forms.Padding(4);
            this.gbGrEf.Name = "gbGrEf";
            this.gbGrEf.Padding = new System.Windows.Forms.Padding(4);
            this.gbGrEf.Size = new System.Drawing.Size(160, 118);
            this.gbGrEf.TabIndex = 8;
            this.gbGrEf.TabStop = false;
            this.gbGrEf.Text = "Grafické efekty";
            // 
            // rbGrEfGW
            // 
            this.rbGrEfGW.AutoSize = true;
            this.rbGrEfGW.Location = new System.Drawing.Point(8, 76);
            this.rbGrEfGW.Margin = new System.Windows.Forms.Padding(4);
            this.rbGrEfGW.Name = "rbGrEfGW";
            this.rbGrEfGW.Size = new System.Drawing.Size(95, 21);
            this.rbGrEfGW.TabIndex = 2;
            this.rbGrEfGW.TabStop = true;
            this.rbGrEfGW.Text = "GW efekty";
            this.rbGrEfGW.UseVisualStyleBackColor = true;
            this.rbGrEfGW.Click += new System.EventHandler(this.rbGrEfGW_Click);
            this.rbGrEfGW.MouseEnter += new System.EventHandler(this.rbGrEfGW_MouseEnter);
            // 
            // rbGrEfOsek
            // 
            this.rbGrEfOsek.AutoSize = true;
            this.rbGrEfOsek.Location = new System.Drawing.Point(7, 48);
            this.rbGrEfOsek.Margin = new System.Windows.Forms.Padding(4);
            this.rbGrEfOsek.Name = "rbGrEfOsek";
            this.rbGrEfOsek.Size = new System.Drawing.Size(128, 21);
            this.rbGrEfOsek.TabIndex = 1;
            this.rbGrEfOsek.TabStop = true;
            this.rbGrEfOsek.Text = "Osekané efekty";
            this.rbGrEfOsek.UseVisualStyleBackColor = true;
            this.rbGrEfOsek.Click += new System.EventHandler(this.rbGrEfOsek_Click);
            this.rbGrEfOsek.MouseEnter += new System.EventHandler(this.rbGrEfOsek_MouseEnter);
            // 
            // rbGrEfStandard
            // 
            this.rbGrEfStandard.AutoSize = true;
            this.rbGrEfStandard.Checked = true;
            this.rbGrEfStandard.Location = new System.Drawing.Point(7, 20);
            this.rbGrEfStandard.Margin = new System.Windows.Forms.Padding(4);
            this.rbGrEfStandard.Name = "rbGrEfStandard";
            this.rbGrEfStandard.Size = new System.Drawing.Size(98, 21);
            this.rbGrEfStandard.TabIndex = 0;
            this.rbGrEfStandard.TabStop = true;
            this.rbGrEfStandard.Text = "Standardní";
            this.rbGrEfStandard.UseVisualStyleBackColor = true;
            this.rbGrEfStandard.Click += new System.EventHandler(this.rbGrEfStandard_Click);
            this.rbGrEfStandard.MouseEnter += new System.EventHandler(this.rbGrEfStandard_MouseEnter);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 2);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(392, 478);
            this.tabControl1.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnCancel.Location = new System.Drawing.Point(257, 487);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(129, 28);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "&Zrušit";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.MouseEnter += new System.EventHandler(this.btnCancel_MouseEnter);
            // 
            // btnApply
            // 
            this.btnApply.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnApply.Location = new System.Drawing.Point(5, 487);
            this.btnApply.Margin = new System.Windows.Forms.Padding(4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(129, 28);
            this.btnApply.TabIndex = 16;
            this.btnApply.Text = "&Uložit změny";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            this.btnApply.MouseEnter += new System.EventHandler(this.btnApply_MouseEnter);
            // 
            // formSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 524);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "formSettings";
            this.ShowInTaskbar = false;
            this.Text = "Nastavení patcheru";
            this.Shown += new System.EventHandler(this.formSettings_Shown);
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gbTrouble.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbPets.ResumeLayout(false);
            this.gbPets.PerformLayout();
            this.gbScreen.ResumeLayout(false);
            this.gbScreen.PerformLayout();
            this.gbGrEf.ResumeLayout(false);
            this.gbGrEf.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnSetupExe;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblWeb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblMap;
        private System.Windows.Forms.Label lblChar;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPortTest;
        private System.Windows.Forms.TextBox tbInfo2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRegReset;
        private System.Windows.Forms.GroupBox gbTrouble;
        private System.Windows.Forms.Button btnPatchReset;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gbPets;
        private System.Windows.Forms.RadioButton rbPetsCzech;
        private System.Windows.Forms.RadioButton rbPetsOff;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.GroupBox gbScreen;
        private System.Windows.Forms.RadioButton rbScreenFullScreen;
        private System.Windows.Forms.RadioButton rbScreenWindow;
        private System.Windows.Forms.GroupBox gbGrEf;
        private System.Windows.Forms.RadioButton rbGrEfOsek;
        private System.Windows.Forms.RadioButton rbGrEfStandard;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.RadioButton rbGrEfGW;
        private System.Windows.Forms.Button btnSdataReset;
        private System.Windows.Forms.CheckBox antishake;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;



    }
}
